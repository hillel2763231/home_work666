# task 1
# Дана довільна строка.
# Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.

def has_two_vowels(word):
    vowels = 'aeiouAEIOU'
    count = 0
    for i in range(len(word) - 1):
        if word[i] in vowels and word[i + 1] in vowels:
            count += 1
    return count

sentence = input("Введіть речення: ")
words = sentence.split()

total_words = 0
for word in words:
    if has_two_vowels(word) > 0:
        total_words += 1

print("Кількість слів з двома голосними літерами підряд:", total_words)
# у цьому завданні я спочатку розробив алгоритм, але не врахував, що спочатку кодом треба зробити так, щоб Пайтон віднайшов голосні
# ( їх треба було списком йому надати, а я думав що для того є якась функція/метод) тому на задачку теж вбив годин 8-10 сумарно... доробив за мене той самий ЧатГеПеТе.

# task 2
#Дана довільна строка. Напишіть код, який знайде в ній і віведе на екран кількість слів, які містять дві голосні літери підряд.
#Є два довільних числа які відповідають за мінімальну і максимальну ціну.
#Є Dict з назвами магазинів і цінами: { "cito": 47.999, "BB_studio" 42.999, "momo": 49.999, "main-service": 37.245, "buy.now": 38.324, "x-store": 37.166, "the_partner": 38.988, "store": 37.720, "rozetka": 38.003}.
# Напишіть код, який знайде і виведе на екран назви магазинів, ціни яких попадають в діапазон між мінімальною і максимальною ціною. Наприклад:
#lower_limit = 35.9
#upper_limit = 37.339
#> match: "x-store", "main-service"

prices = {
    "cito": 47.999,
    "BB_studio": 42.999,
    "momo": 49.999,
    "main-service": 37.245,
    "buy.now": 38.324,
    "x-store": 37.166,
    "the_partner": 38.988,
    "store": 37.720,
    "rozetka": 38.003
}

lower_limit = 35.9
upper_limit = 37.339

matching_stores = []

for store, price in prices.items():
    if lower_limit <= price <= upper_limit: #вирішив до цієї операції, а далі не знав як зпівставити результат виразу із діктом... Допоміг в останній день мій кореш ЧатГеПете.
        matching_stores.append(store)

if matching_stores:
    print("Match:", ", ".join(matching_stores))
else:
    print("No matching stores found.")

