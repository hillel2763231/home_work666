# task 1
# Доопрацюйте класс Point так, щоб в атрибути x та y обʼєктів цього класу можна було записати тільки обʼєкти класу int або float

# task 2
# Створіть класс Triangle (трикутник), який задається (ініціалізується) трьома точками (обʼєкти классу Point).
# Реалізуйте перевірку даних, аналогічно до класу Line. Визначет метод, що містить площу трикутника.
# Для обчислень можна використати формулу Герона (https://en.wikipedia.org/wiki/Heron%27s_formula)

class Point:
    def __init__(self, x, y):
        if isinstance(x, (int, float)) and isinstance(y, (int, float)):
            self.x = x
            self.y = y
        else:
            raise ValueError("Атрибути x й y мають бути integer або float")

class Triangle:
    def __init__(self, point1, point2, point3):
        if isinstance(point1, Point) and isinstance(point2, Point) and isinstance(point3, Point):
            self.point1 = point1
            self.point2 = point2
            self.point3 = point3
        else:
            raise ValueError("Всі 3 точки мають відноситися до класу Point")

    def calculate_distance(self, point1, point2):
        return ((point1.x - point2.x) ** 2 + (point1.y - point2.y) ** 2) ** 0.5

    def calculate_area(self):
        a = self.calculate_distance(self.point1, self.point2)
        b = self.calculate_distance(self.point2, self.point3)
        c = self.calculate_distance(self.point3, self.point1)
        s = (a + b + c) / 2
        area = (s * (s - a) * (s - b) * (s - c)) ** 0.5
        return area

point1 = Point(0, 0)
point2 = Point(4, 0)
point3 = Point(0, 3)

triangle = Triangle(point1, point2, point3)
area = triangle.calculate_area()
print("Площа трикутника:", area)