# TASK
#Напишіть гру "rock-paper-scissors-lizard-Spock", посилання на правила.
#Ввід даних гравцем - ченез input
#Рекомендую подумати над такими класами як Player, GameFigure, Game.
#Памʼятайте про чистоту і простоту коду (DRY, KISS), коментарі та докстрінги.

import random

# Клас для гравця
class Player:
    def __init__(self, name):
        self.name = name
        self.choice = None

    def choose_gesture(self):
        pass

# Клас для користувача
class User(Player):
    def choose_gesture(self):
        gestures = ["Камінь", "Ножиці", "Папір", "Ящірка", "Спок"]
        print("Доступні жести: 1 - Камінь, 2 - Ножиці, 3 - Папір, 4 - Ящірка, 5 - Спок")
        choice_num = int(input(f"{self.name}, виберіть номер жесту: "))
        self.choice = gestures[choice_num - 1]

# Клас для штучного інтелекту
class AI(Player):
    def choose_gesture(self):
        gestures = ["Камінь", "Ножиці", "Папір", "Ящірка", "Спок"]
        self.choice = random.choice(gestures)

# Клас для гри
class Game:
    def __init__(self):
        self.player1 = User("Гравець")
        self.player2 = AI("Штучний інтелект")

    def determine_winner(self):
        if self.player1.choice == self.player2.choice:
            return "Нічия"
        elif (
            (self.player1.choice == "Камінь" and (self.player2.choice == "Ножиці" or self.player2.choice == "Ящірка"))
            or (self.player1.choice == "Ножиці" and (self.player2.choice == "Папір" or self.player2.choice == "Ящірка"))
            or (self.player1.choice == "Папір" and (self.player2.choice == "Камінь" or self.player2.choice == "Спок"))
            or (self.player1.choice == "Ящірка" and (self.player2.choice == "Спок" or self.player2.choice == "Папір"))
            or (self.player1.choice == "Спок" and (self.player2.choice == "Ножиці" or self.player2.choice == "Камінь"))
        ):
            return self.player1.name
        else:
            return self.player2.name

    def play_game(self):
        print("Гра 'Камінь-Ножиці-Папір-Ящірка-Спок' розпочалася!")
        self.player1.choose_gesture()
        self.player2.choose_gesture()
        print(f"{self.player1.name} вибрав: {self.player1.choice}")
        print(f"{self.player2.name} вибрав: {self.player2.choice}")
        winner = self.determine_winner()
        print(f"Переможець: {winner}")


# Запуск гри
if __name__ == "__main__":
    game = Game()
    game.play_game()





