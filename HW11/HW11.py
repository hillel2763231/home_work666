#Подключіться до API НБУ ( документація тут https://bank.gov.ua/ua/open-data/api-dev ), отримайте теперішній курс валют и запишіть його в TXT-файл в такому форматі:
#"[дата, на яку актуальний курс]"
#1. [назва валюти 1] to UAH: [значення курсу валюти 1]
#2. [назва валюти 2] to UAH: [значення курсу валюти 2]
#3. [назва валюти 3] to UAH: [значення курсу валюти 3]
#...
#n. [назва валюти n] to UAH: [значення курсу валюти n]
#опціонально передбачте для користувача можливість обирати дату, на яку він хоче отримати курс

#P.S. За можливості зробіть все за допомогою ООП

import requests

class NBUAPI:
    BASE_URL = 'https://bank.gov.ua/NBUStatService/v1/statdirectory'

    def get_currency_exchange_rate(self, currency_code, date=None):
        url = f'{self.BASE_URL}?valcode={currency_code}&json'
        if date:
            url += f'&date={date}'

        response = requests.get(url)
        if response.status_code == 200:
            data = response.json()
            if data:
                return data[0]['rate']
        return None

def save_currency_rates_to_file(filename, date=None):
    nbu_api = NBUAPI()

    with open(filename, 'w') as file:
        if date:
            file.write(f"[дата, на яку актуальний курс]: {date}\n")
        else:
            file.write("[дата, на яку актуальний курс]: Теперішній час\n")

        currencies = ["USD", "EUR", "GBP"]  # Додайте всі валюти, які вас цікавлять
        for i, currency in enumerate(currencies, start=1):
            rate = nbu_api.get_currency_exchange_rate(currency, date)
            if rate is not None:
                file.write(f"{i}. {currency} to UAH: {rate}\n")

if __name__ == "__main__":
    date = input("Введіть дату (у форматі YYYYMMDD) або натисніть Enter для отримання теперішніх курсів: ")
    if date and not date.isdigit() or len(date) != 8:
        print("Неправильний формат дати. Введіть дату у форматі YYYYMMDD.")
    else:
        filename = "currency_rates.txt"
        save_currency_rates_to_file(filename, date)
        print(f"Курси валют збережено в файл {filename}.")