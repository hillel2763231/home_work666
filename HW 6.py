# task 1
# Напишіть декоратор, який вимірює час виконання функції:

import time

def measure_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Ф-кція '{func.__name__}' займає {execution_time:.6f} секунди на виконання.")
        return result
    return wrapper

@measure_time
def smth_func():

    time.sleep(2)

smth_func()