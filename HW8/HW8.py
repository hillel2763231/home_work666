#task #1
#Створіть клас "Транспортний засіб" та підкласи "Автомобіль", "Літак", "Корабель", наслідувані від "Транспортний засіб".
# Наповніть класи атрибутами на свій розсуд.

#task #2
#Створіть обʼєкти класів "Автомобіль", "Літак", "Корабель".



class Vehicle:
    '''Транспортний засіб'''
    def __init__(self, make, model, year):
        self.make = make #марка
        self.model = model #модель
        self.year = year #рік виробництва

class Car(Vehicle):
    '''Машина'''
    def __init__(self, make, model, year, num_doors):
        super().__init__(make, model, year)
        self.num_doors = num_doors

class Plane(Vehicle):
    '''Літак'''
    def __init__(self, make, model, year, max_altitude):
        super().__init__(make, model, year)
        self.max_altitude = max_altitude

class Ship(Vehicle):
    '''Корабель'''
    def __init__(self, make, model, year, max_speed):
        super().__init__(make, model, year)
        self.max_speed = max_speed


my_car = Car("Toyota", "Camry", 2022, 4)
my_plane = Plane("Boeing", "747", 2021, 35000)
my_ship = Ship("Royal Caribbean", "Freedom of the Seas", 2019, 24)







