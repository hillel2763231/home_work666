#decorator

import time
def measure_time(func):
    def wrapper(*args, **kwargs):
        start_time = time.time()
        result = func(*args, **kwargs)
        end_time = time.time()
        execution_time = end_time - start_time
        print(f"Ф-кція '{func.__name__}' займає {execution_time:.6f} секунди на виконання.")
        return result
    return wrapper

#Function
@measure_time
def function(x):
    while True:
        time.sleep(1.5)
        break
    while x.isdigit() == True:
        if x.count('7') >= 1 :
            print(f'Вам {x} рочків - Вам пощастить!')
            return x

        elif int(x) < 6 :
            if int(x) < 6 and x.count('1'):
                print(f'Тобі ж {x} рік! Де твої батьки?')
            elif int(x) < 6 and x.count('5'):
                print(f'Тобі ж {x} років! Де твої батьки?')
            else :
                print(f'Тобі ж {x} роки! Де твої батьки?')
            return x

        elif int(x) < 16 :
            print(f'Тобі лише {x} років, а це є фільм для дорослих!')
            return x

        elif int(x) > 65 :
            if int(x) > 65 and x.count('1'):
                print(f'Вам {x} рік? Покажіть пенсійне посвідчення!')
                return
            if int(x) > 65 and x.count('2') or x.count('3') or x.count('4') :
                print(f'Вам {x} роки? Покажіть пенсійне посвідчення!')
            else:
                print(f'Вам {x} років? Покажіть пенсійне посвідчення!')
            return x

        elif int(x) >= 16 and int(x) <= 65 and x.count('1'):
            print(f'Незважаючи на те, що Вам {x} рік, білетів всеодно немає!')

        elif int(x) >= 16 and int(x) <= 65 or  x.count('2') or x.count('3') or x.count('4'):
            print(f'Незважаючи на те, що Вам {x} роки, білетів всеодно немає!')

        else :
            print(f'Незважаючи на те, що Вам {x} років, білетів всеодно немає!')
            return x

            break

    while x.isdigit() == False :
        print('ВВОДИТИ МОЖНА ЛИШЕ ЦИФРУ АБО ЧИСЛО!')

        break
        return



age = function(input('Вітаю! Введіть Ваш вік (Вводити тільки цифру/число) :'))

#decorator



